# demo-watson1
IBM Watson language service demo.


## References

* [Conversation](https://www.ibm.com/watson/developercloud/conversation.html)
* [Natural Language Classifier](https://www.ibm.com/watson/developercloud/nl-classifier.html)
* [Natural Language Understanding](https://www.ibm.com/watson/developercloud/natural-language-understanding.html)


